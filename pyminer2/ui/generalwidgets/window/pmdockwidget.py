from PyQt5.QtGui import QCloseEvent, QResizeEvent
from PyQt5.QtWidgets import QDockWidget, QMainWindow


class PMGDockWidget(QDockWidget):
    def __init__(self, text='', parent: QMainWindow = None):
        super().__init__(text, parent)
        self.parent = parent


class PMDockWidget(PMGDockWidget):

    def closeEvent(self, event: QCloseEvent):
        from pyminer2.pmutil import get_main_window
        self.hide()
        event.accept()
        get_main_window().refresh_view_configs()
